package sk.stuba.fei.oop.support;

public class NullChecker {

    public static void checkNull(Object obj)throws NullPointerException{
        if(obj == null)
            throw new NullPointerException();
    }

}
