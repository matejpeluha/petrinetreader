package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.support.IntPoint;
import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.arcs.Arc;
import sk.stuba.fei.oop.petrinet.arcs.BreakPoint;
import sk.stuba.fei.oop.petrinet.arcs.NormalArc;
import sk.stuba.fei.oop.petrinet.arcs.ResetArc;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class ArcPainter implements InterfaceArcPainter {
    private Graphics2D g;
    private Arc arc;
    private BreakPoint breakPoint;

    private final int sizeX = 40;
    private final int sizeY = 40;
    private final int ARR_SIZE = 8;

    private double dx;
    private double dy;
    private double angle;
    private int len;
    private IntPoint startPoint, endPoint;


    public ArcPainter(Graphics g1, Arc arc)throws NullPointerException{
        NullChecker.checkNull(g1);

        this.g = (Graphics2D) g1.create();
        this.g.setStroke(new BasicStroke(2));

        setAtributes(arc);
    }

    private void updateAtributes(){
        this.dx = this.endPoint.getX() - this.startPoint.getX();
        this.dy = this.endPoint.getY() - this.startPoint.getY();

        this.angle = Math.atan2(this.dy, this.dx);
        this.len = (int) Math.sqrt(this.dx * this.dx + this.dy * this.dy);
    }

    private void paintArrow(){
        updateAtributes();

        int x = this.startPoint.getX();
        int y = this.startPoint.getY();

        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.concatenate(AffineTransform.getRotateInstance(this.angle));
        this.g.transform(at);

        this.g.drawLine(0, 0, this.len, 0);
        this.g.fillPolygon(new int[] {this.len, this.len - this.ARR_SIZE, this.len - this.ARR_SIZE, this.len},
                new int[] {0, -ARR_SIZE, this.ARR_SIZE, 0}, 4);

    }

    private void calculateDx(){
        int startX = this.startPoint.getX();
        int endX = this.endPoint.getX();

        if(startX < endX)
            this.dx -= 5;
        else
            this.dx += 5;
    }

    private void calculateDY(){
        int startY = this.startPoint.getY();
        int endY = this.endPoint.getY();

        if(startY < endY)
            this.dy -= 5;
        else
            this.dy += 5;
    }

    private void paintDoubleArrow(){
        paintArrow();

        calculateDx();
        calculateDY();

        this.len = (int) Math.sqrt(this.dx * this.dx + this.dy * this.dy);

        this.g.fillPolygon(new int[] {this.len, this.len - this.ARR_SIZE, this.len - this.ARR_SIZE, this.len},
                new int[] {0, -ARR_SIZE, this.ARR_SIZE, 0}, 4);
    }

    private void calculateStartX(int endX){
        int sizeX = this.sizeX + this.sizeX / 2;
        int startX = this.startPoint.getX();

        if(startX <= endX - sizeX ) {
            startX += this.sizeX / 2;
        }
        else if(startX >= endX + sizeX ) {
            startX -= this.sizeX / 2;
        }

        this.startPoint.setX(startX);
    }

    private void calculateStartY(int endY){
        int sizeY = this.sizeY + this.sizeY / 2;
        int startY = this.startPoint.getY();

        if(startY <= endY - sizeY ) {
            startY += this.sizeY / 2;
        }
        else if(startY >= endY + sizeY ) {
            startY -= this.sizeY / 2;
        }

        this.startPoint.setY(startY);
    }


    private void calculateStartPoint(IntPoint point)throws NullPointerException{
        NullChecker.checkNull(point);

        calculateStartX(point.getX());
        calculateStartY(point.getY());
    }

    private void calculateEndX(){
        int sizeX = this.sizeX + this.sizeX / 2;
        int startX = this.startPoint.getX();
        int endX = this.endPoint.getX();

        if(startX <= endX - sizeX ) {
            endX -= this.sizeX / 2 ;
        }
        else if(startX >= endX + sizeX ) {
            endX += this.sizeX / 2;
        }

        this.endPoint.setX(endX);
    }

    private void calculateEndY(){
        int sizeY = this.sizeY + this.sizeY / 2;
        int startY = this.startPoint.getY();
        int endY = this.endPoint.getY();

        if(startY <= endY - sizeY ) {
            endY -= this.sizeY / 2;
        }
        else if(startY >= endY + sizeY ) {
            endY += this.sizeY / 2;
        }

        this.endPoint.setY(endY);
    }

    private void calculateEndPoint(){
        calculateEndX();
        calculateEndY();
    }

    private void drawBreakLine(){
        int breakX = this.breakPoint.getX();
        int breakY = this.breakPoint.getY();
        IntPoint point = new IntPoint(breakX, breakY);

        calculateStartPoint(point);

        int startX = this.startPoint.getX();
        int startY = this.startPoint.getY();

        this.g.drawLine(startX, startY, breakX, breakY);
    }

    private void calculateStartPointAfterBreakPoint(){
        int breakX = this.breakPoint.getX();
        int breakY = this.breakPoint.getY();

        this.startPoint.setY(breakY);
        this.startPoint.setX(breakX);
    }

    @Override
    public void paintArc(){
        if(this.breakPoint != null){
            drawBreakLine();
            calculateStartPointAfterBreakPoint();
        }
        else
            calculateStartPoint(this.endPoint);

        calculateEndPoint();


        if(arc instanceof NormalArc)
            paintArrow();
        else if(arc instanceof ResetArc)
            paintDoubleArrow();

    }

    @Override
    public void setAtributes(Arc arc){
        NullChecker.checkNull(arc);

        int startX = arc.getOut().getX();
        int startY = arc.getOut().getY();
        int endX = arc.getIn().getX();
        int endY = arc.getIn().getY();

        this.startPoint = new IntPoint(startX, startY);
        this.endPoint = new IntPoint(endX, endY);

        this.arc = arc;

        this.breakPoint = arc.getBreakPoint();

       updateAtributes();
    }
}
