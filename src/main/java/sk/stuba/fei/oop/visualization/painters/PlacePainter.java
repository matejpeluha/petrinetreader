package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Vertex;
import sk.stuba.fei.oop.support.IntPoint;

import java.awt.*;

public class PlacePainter extends VertexPainter implements InterfacePlacePainter {
    private Place place;
    private int markingInt;

    public PlacePainter(Graphics g, Place place){
        super(g, place);
    }

    private String intToString(int num){
        return "" + num;
    }

    @Override
    public void paintPlace(){
        String markingStr = intToString(this.markingInt);
        int fontSize = 2;
        int placeX = this.location.getX();
        int placeY = this.location.getY();
        IntPoint placeLocation = new IntPoint(this.place.getX(), this.place.getY());

        this.g.setStroke(new BasicStroke(fontSize));
        this.g.drawOval(placeX, placeY, this.sizeX, this.sizeY);
        this.g.drawString(markingStr, place.getX() - 5, place.getY() + 5);

        drawCenteredStringForVertex(this.place.getName(), placeLocation);
    }

    @Override
    public void setAttributes(Vertex place){
        super.setAttributes(place);

        this.place = (Place) place;
        this.markingInt = this.place.getMarking();
    }
}
