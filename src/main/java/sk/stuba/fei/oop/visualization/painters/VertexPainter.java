package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.support.IntPoint;
import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

import java.awt.*;

public abstract class VertexPainter {
    Graphics2D g;
    protected IntPoint location;
    protected final int sizeX = 40;
    protected final int sizeY = 40;

    public VertexPainter(){}

    public VertexPainter(Graphics g, Vertex vertex) {
        NullChecker.checkNull(g);
        this.g = (Graphics2D) g;

        setAttributes(vertex);
    }


    protected void drawCenteredStringForVertex(String text, IntPoint vertexLocation){
        NullChecker.checkNull(text);

        if(text.equals(""))
            return;

        int widthText = this.g.getFontMetrics().stringWidth(text);
        int x = vertexLocation.getX() - widthText / 2;
        int y = vertexLocation.getY() + 40;
        int fontSize = 13;

        this.g.setFont(new Font("Serif", Font.BOLD, fontSize));

        this.g.setColor(new Color(0xAC979797, true));
        this.g.fillRect(x - 3 ,y - 13 , widthText + 5, 16);

        this.g.setColor(Color.BLACK);
        this.g.drawString(text, x, y);
    }

    public void setAttributes(Vertex vertex){
        NullChecker.checkNull(vertex);

        int x = vertex.getX() - this.sizeX / 2;
        int y = vertex.getY() - this.sizeY / 2;

        this.location = new IntPoint(x, y);
    };

}
