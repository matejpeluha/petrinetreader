package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.arcs.Arc;

public interface InterfaceArcPainter {
    void paintArc();
    void setAtributes(Arc arc);
}

