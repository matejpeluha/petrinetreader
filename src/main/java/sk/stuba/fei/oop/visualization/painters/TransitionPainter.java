package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.vertexes.Transition;
import sk.stuba.fei.oop.petrinet.vertexes.Vertex;
import sk.stuba.fei.oop.support.IntPoint;

import java.awt.*;

public class TransitionPainter extends VertexPainter implements InterfaceTransitionPainter {
    private Transition transition;

    public TransitionPainter(Graphics g, Transition transition){
        super(g, transition);
    }

    private void setUpFireableGraphics(){
        int transitionX = this.location.getX();
        int transitionY = this.location.getY();

        this.g.setColor(new Color(0x07FF4E));
        this.g.fillRect(transitionX, transitionY, this.sizeX, this.sizeY);
        this.g.setColor(new Color(0x0F8600));
    }

    private void setUpNONFireableGraphics(){
        this.g.setColor(Color.RED);
    }

    private void setUpNewGraphics(){
        int paintWidth = 2;
        this.g.setStroke(new BasicStroke(paintWidth));

        if(this.transition.isFireable()) {
            setUpFireableGraphics();
        }

        else {
            setUpNONFireableGraphics();
        }
    }

    private void setUpDefaultGraphics(){
        int paintWidth = 1;

        this.g.setColor(Color.BLACK);
        this.g.setStroke(new BasicStroke(paintWidth));
    }

    @Override
    public void paintTransition(){
        int rectangleX = this.location.getX();
        int rectangleY = this.location.getY();
        IntPoint transitionLocation = new IntPoint(this.transition.getX(), this.transition.getY());

        setUpNewGraphics();

        this.g.drawRect(rectangleX, rectangleY, this.sizeX, this.sizeY);

        drawCenteredStringForVertex(this.transition.getName(), transitionLocation);

        setUpDefaultGraphics();
    }

    @Override
    public void setAttributes(Vertex transition){
        super.setAttributes(transition);

        this.transition = (Transition) transition;
    }
}
