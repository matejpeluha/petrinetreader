package sk.stuba.fei.oop.visualization;

import sk.stuba.fei.oop.generated.Document;
import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.Petrinet;


import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MyFrame extends JFrame implements ActionListener {
    private JButton buttonImport = new JButton("Import");
    private Petrinet net = null;

    public MyFrame(Petrinet net){
        super();

        Dimension dimension = new Dimension(1500, 700);
        setSize(dimension);
        setLayout(new BorderLayout());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new Closer());

        this.buttonImport.addActionListener(this);

        Panel panel = new Panel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 0));

        this.net = net;
        MyCanvas canvas = new MyCanvas(this.net);


        panel.add(this.buttonImport);
        add(panel, "North");
        add(canvas, "Center");

        setVisible(true);
    }


    private Document importDocument(String dir) throws NullPointerException{
        NullChecker.checkNull(dir);

        Document document = new Document();

        try {
            File file = new File(dir);
            JAXBContext jaxbContext = JAXBContext.newInstance(Document.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            document = (Document) jaxbUnmarshaller.unmarshal(file);

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return document;
    }


    private void createPetriNet(String dir){
        Document document = importDocument(dir);
        Converter converter = new Converter();
        this.net = converter.convert(document);
    }


    private void tryCreatePetriNet(String dir){
        try {
            createPetriNet(dir);
            new MyFrame(this.net);
            dispose();
        }
        catch(Exception exception){
            new MyFrame(null);
            dispose();
        }
    }

    private FileFilter createFilter(){
        FileFilter filter = new FileFilter() {
            public String getDescription() {
                return ".xml";
            }

            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else {
                    String filename = f.getName().toLowerCase();
                    return filename.endsWith(".xml") ;
                }
            }
        };
        return filter;
    }

    private String choosePetriNetXmlDirection() {
        String dir = System.getProperty("user.dir");
        JFileChooser fileChooser = new JFileChooser(dir + "\\src\\main\\resources");

        FileFilter filter = createFilter();

        fileChooser.setFileFilter(filter);

        fileChooser.showSaveDialog(null);

        return fileChooser.getSelectedFile().getAbsolutePath();
    }

    private void createNewMyFrameWithOldPetriNet(){
        new MyFrame(this.net);
        dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonImport) {
            try {
                String dir = choosePetriNetXmlDirection();
                tryCreatePetriNet(dir);
            }
            catch(Exception exception){
                createNewMyFrameWithOldPetriNet();
            }
        }


    }
}
