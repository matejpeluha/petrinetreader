package sk.stuba.fei.oop.visualization;


import sk.stuba.fei.oop.petrinet.Petrinet;
import sk.stuba.fei.oop.petrinet.arcs.Arc;

import sk.stuba.fei.oop.petrinet.arcs.NormalArc;
import sk.stuba.fei.oop.petrinet.arcs.ResetArc;
import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Transition;
import sk.stuba.fei.oop.support.IntPoint;
import sk.stuba.fei.oop.visualization.painters.ArcPainter;
import sk.stuba.fei.oop.visualization.painters.PlacePainter;
import sk.stuba.fei.oop.visualization.painters.TransitionPainter;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;


public class MyCanvas extends Canvas {
    private Petrinet net;
    private Graphics g;

    public MyCanvas(Petrinet net){
        this.net = net;
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        this.g = g;

        if(this.net == null){
            textNoPetriNet();
        }
        else {
            drawAllArcs();
            drawAllTransitions();
            drawAllPlaces();
        }

    }

    private void textNoPetriNet(){
        String text = "Importuj platnu Petriho siet.";
        int fontSize = 40;
        Font serifFont = new Font("Serif", Font.BOLD, fontSize);
        IntPoint point = new IntPoint(40, 40);

        this.g.setFont(serifFont);
        this.g.drawString(text, point.getX(), point.getY() );
    }

    private void drawOneTransition(Transition transition){
        TransitionPainter transitionPainter = new TransitionPainter(this.g, transition);

        transitionPainter.paintTransition();
    }


    private void drawAllTransitions(){
        HashMap<Long, Transition> transitions = this.net.getTransitions();

        for (HashMap.Entry oneSet : transitions.entrySet()){
            Transition transition = (Transition) oneSet.getValue();
            drawOneTransition(transition);
        }
    }

    private void drawOnePlace(Place place){
        PlacePainter placePainter = new PlacePainter(this.g, place);

        placePainter.paintPlace();
    }


    private void drawAllPlaces(){
        HashMap<Long, Place> places = this.net.getPlaces();

        for (HashMap.Entry oneSet : places.entrySet()){
            Place place = (Place) oneSet.getValue();
            drawOnePlace(place);
        }
    }


    private void drawOneArc(Arc arc){
        ArcPainter arcPainter = new ArcPainter(this.g, arc);

        arcPainter.paintArc();
    }

    private void drawAllResetArcs(ArrayList<ResetArc> resetArcs){
        for(ResetArc arc : resetArcs){
            drawOneArc(arc);
        }
    }

    private void drawAllNormalArcs(ArrayList<NormalArc> normalArcs){
        for(NormalArc arc : normalArcs){
            drawOneArc(arc);
        }
    }

    private void drawAllArcs(){
        ArrayList<NormalArc> normalArcs = this.net.getArcs().getNormalArcs();
        ArrayList<ResetArc> resetArcs = this.net.getArcs().getResetArcs();

        drawAllResetArcs(resetArcs);
        drawAllNormalArcs(normalArcs);
    }

}
