package sk.stuba.fei.oop.visualization;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Closer extends WindowAdapter

{   public void windowClosing (WindowEvent e) {

    System.exit(0);
}

}