package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

public interface InterfacePlacePainter extends InterfaceVertexPainter {
    void paintPlace();
    void setAttributes(Vertex place);
}
