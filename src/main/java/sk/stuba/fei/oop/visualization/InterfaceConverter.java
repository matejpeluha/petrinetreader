package sk.stuba.fei.oop.visualization;

import sk.stuba.fei.oop.generated.Document;
import sk.stuba.fei.oop.petrinet.Petrinet;

public interface InterfaceConverter {
    Petrinet convert(Document doc);
}
