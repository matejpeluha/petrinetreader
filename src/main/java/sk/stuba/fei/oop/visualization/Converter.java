package sk.stuba.fei.oop.visualization;

import sk.stuba.fei.oop.generated.Arc;
import sk.stuba.fei.oop.generated.Document;
import sk.stuba.fei.oop.generated.Place;
import sk.stuba.fei.oop.generated.BreakPoint;
import sk.stuba.fei.oop.generated.Transition;
import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.Petrinet;


import java.util.ArrayList;
import java.util.List;

public class Converter implements InterfaceConverter{
    private Petrinet net = new Petrinet();
    List<Transition> transitionsDoc = new ArrayList<Transition>();
    List<Place> placesDoc = new ArrayList<Place>();
    List<Arc> arcsDoc = new ArrayList<Arc>();

    private void readDocument(Document doc){
        NullChecker.checkNull(doc);

        this.transitionsDoc = doc.getTransition();
        this.placesDoc = doc.getPlace();
        this.arcsDoc = doc.getArc();
    }

    private sk.stuba.fei.oop.petrinet.vertexes.Transition convertTransition(Transition transitionDoc)throws NullPointerException{
        NullChecker.checkNull(transitionDoc);

        int id = transitionDoc.getId();
        String name = transitionDoc.getLabel();
        int x = transitionDoc.getX();
        int y = transitionDoc.getY();

        return new sk.stuba.fei.oop.petrinet.vertexes.Transition(id, name, x, y);
    }

    private sk.stuba.fei.oop.petrinet.vertexes.Place convertPlace(Place placeDoc) throws NullPointerException{
        NullChecker.checkNull(placeDoc);

        int id = placeDoc.getId();
        String name = placeDoc.getLabel();
        int x = placeDoc.getX();
        int y = placeDoc.getY();
        int marking = placeDoc.getTokens();

        return new sk.stuba.fei.oop.petrinet.vertexes.Place(id, name, marking, x, y);
    }

    private void addOneTransition(Transition transitionDoc){
        sk.stuba.fei.oop.petrinet.vertexes.Transition transition;

        transition = convertTransition(transitionDoc);

        this.net.addTransition(transition);
    }

    private void addAllTransitions(){
        for(Transition transitionDoc : this.transitionsDoc) {
            addOneTransition(transitionDoc);
        }
    }

    private void addOnePlace(Place placeDoc){
        sk.stuba.fei.oop.petrinet.vertexes.Place place;

        place = convertPlace(placeDoc);

        this.net.addPlace(place);
    }

    private void addAllPlaces(){
        for(Place placeDoc : this.placesDoc){
            addOnePlace(placeDoc);
        }
    }

    private sk.stuba.fei.oop.petrinet.arcs.BreakPoint convertBreakPoint(BreakPoint breakPointDoc){
        if(breakPointDoc == null)
            return null;

        int x = breakPointDoc.getX();
        int y = breakPointDoc.getY();

        return new sk.stuba.fei.oop.petrinet.arcs.BreakPoint(x, y);
    }

    private void addResetArc(Arc arcDoc)throws NullPointerException{
        NullChecker.checkNull(arcDoc);

        long placeId = arcDoc.getSourceId();
        long transitionId = arcDoc.getDestinationId();
        sk.stuba.fei.oop.petrinet.vertexes.Place place = this.net.getPlaces().get(placeId);
        sk.stuba.fei.oop.petrinet.vertexes.Transition transition = this.net.getTransitions().get(transitionId);
        sk.stuba.fei.oop.petrinet.arcs.BreakPoint breakPoint = convertBreakPoint(arcDoc.getBreakPoint());

        this.net.addPTResetArc(place, transition, breakPoint);
    }

    private void addPTNormalArc(long outID, long inID, int multiplicity, BreakPoint breakPointDoc) {
        sk.stuba.fei.oop.petrinet.vertexes.Place place = this.net.getPlaces().get(outID);
        sk.stuba.fei.oop.petrinet.vertexes.Transition transition = this.net.getTransitions().get(inID);
        sk.stuba.fei.oop.petrinet.arcs.BreakPoint breakPoint = convertBreakPoint(breakPointDoc);

        this.net.addPTNormalArc(place, transition, multiplicity, breakPoint);
    }

    private void addTPNormalArc(long outID, long inID, int multiplicity, BreakPoint breakPointDoc){
        sk.stuba.fei.oop.petrinet.vertexes.Transition  transition= this.net.getTransitions().get(outID);
        sk.stuba.fei.oop.petrinet.vertexes.Place place = this.net.getPlaces().get(inID);
        sk.stuba.fei.oop.petrinet.arcs.BreakPoint breakPoint = convertBreakPoint(breakPointDoc);

        this.net.addTPNormalArc(transition, place, multiplicity, breakPoint);
    }

    private void addNormalArc(Arc arcDoc)throws NullPointerException{
        NullChecker.checkNull(arcDoc);

        long outID = arcDoc.getSourceId();
        long inID = arcDoc.getDestinationId();
        int multilicity = arcDoc.getMultiplicity();
        BreakPoint breakPointDoc = arcDoc.getBreakPoint();

        if( net.getPlaces().containsKey(outID) && net.getTransitions().containsKey(inID) )
            addPTNormalArc(outID, inID, multilicity, breakPointDoc);

        else if( net.getTransitions().containsKey(outID) && net.getPlaces().containsKey(inID) )
            addTPNormalArc(outID, inID, multilicity, breakPointDoc);
    }


    private void addOneArc(Arc arcDoc){
        if(arcDoc.getType().equals("regular"))
            addNormalArc(arcDoc);

        else if(arcDoc.getType().equals("reset"))
            addResetArc(arcDoc);

    }

    private void addAllArcs(){
        for(Arc arcDoc : this.arcsDoc){
            addOneArc(arcDoc);
        }
    }

    @Override
    public Petrinet convert(Document doc){
        readDocument(doc);

        addAllTransitions();
        addAllPlaces();
        addAllArcs();

        this.transitionsDoc.clear();
        this.placesDoc.clear();
        this.arcsDoc.clear();

        return this.net;
    }
}
