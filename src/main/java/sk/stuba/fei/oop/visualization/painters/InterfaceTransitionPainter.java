package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

public interface InterfaceTransitionPainter extends InterfaceVertexPainter {
    void setAttributes(Vertex transition);
    void paintTransition();
}
