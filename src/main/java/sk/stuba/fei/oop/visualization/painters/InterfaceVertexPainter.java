package sk.stuba.fei.oop.visualization.painters;

import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

public interface InterfaceVertexPainter {
    void setAttributes(Vertex vertex);
}
