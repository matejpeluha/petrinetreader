package sk.stuba.fei.oop.petrinet.arcs;

import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

public class ResetArc extends Arc {
    public ResetArc(Vertex out, Vertex in) throws IllegalArgumentException {
        super(out, in);

        //tato vynimka je zaobstarana uz v Arc, aby sa vobec nevytvorilo priradenie out a in
        /*
        if(out instanceof Transition)
            throw new IllegalArgumentException("Reset hrana nesmie vychadzat z prechodu a smerovat do miesta.");
            */

    }

    @Override
    public int getMultiplicity() {
        Place place = (Place) this.out;
        return place.getMarking();
    }
}
