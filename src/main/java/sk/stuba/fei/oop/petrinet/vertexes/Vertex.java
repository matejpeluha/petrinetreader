package sk.stuba.fei.oop.petrinet.vertexes;

import sk.stuba.fei.oop.support.IntPoint;
import sk.stuba.fei.oop.support.NullChecker;

abstract public class Vertex {
    protected long ID;
    protected String name;
    private IntPoint point;

    public Vertex(long ID, String name, int x, int y) throws IllegalArgumentException {
        setID(ID);
        setName(name);
        setPoint(x, y);
    }

    public Vertex(long ID, String name, IntPoint point) throws IllegalArgumentException {
        setID(ID);
        setName(name);
        setPoint(point);
    }


    public void setID(long ID){
        if(ID < 0)
            throw new IllegalArgumentException("ID nesmie byt zaporne.");

        this.ID = ID;
    }

    public long getID() {
        return ID;
    }

    public void setName(String name){
        NullChecker.checkNull(name);

        this.name = name;
    }

    public void setPoint(IntPoint point){
        NullChecker.checkNull(point);

        if(point.getY() < 0 || point.getX() < 0)
            throw new IllegalArgumentException("Poloha nesmie byt zaporna.");

        this.point = point;
    }

    public void setPoint(int x, int y){
        IntPoint point = new IntPoint(x, y);
        setPoint(point);
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return this.point.getX();
    }

    public int getY() {
        return this.point.getY();
    }
}
