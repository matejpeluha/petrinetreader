package sk.stuba.fei.oop.petrinet;

import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.arcs.BreakPoint;
import sk.stuba.fei.oop.petrinet.arcs.ListArcs;
import sk.stuba.fei.oop.petrinet.arcs.NormalArc;
import sk.stuba.fei.oop.petrinet.arcs.ResetArc;
import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Transition;

import java.util.ArrayList;
import java.util.HashMap;


public class Petrinet implements InterfacePetrinet {
    private HashMap<Long, Place> places = new HashMap<Long, Place>();
    private HashMap<Long, Transition> transitions = new HashMap<Long, Transition>();
    private ListArcs arcs = new ListArcs();

    public HashMap<Long, Place> getPlaces() {
        return places;
    }

    public ListArcs getArcs() {
        return arcs;
    }


    @Override
    public void addPlace(Place place) throws IllegalArgumentException, NullPointerException{
        long id;

        NullChecker.checkNull(place);

        id = place.getID();

        if (this.places.containsKey(id))
            throw new IllegalArgumentException("Miesto " + place.getName() + " a miesto "
                    + this.places.get(id).getName() + " maju rovnake ID.");

        else if (this.transitions.containsKey(id))
            throw new IllegalArgumentException("Miesto " + place.getName() + " a prechod "
                    + this.transitions.get(id).getName() + " maju rovnake ID.");

        this.places.put(id, place);
    }

    @Override
    public void addTransition(Transition transition) throws IllegalArgumentException, NullPointerException {
        long id;

        NullChecker.checkNull(transition);

        id = transition.getID();

        if (this.transitions.containsKey(id))
            throw new IllegalArgumentException("Prechod " + transition.getName() + " a prechod "
                    + this.transitions.get(id).getName() + " maju rovnake ID.");

        else if (this.places.containsKey(id))
            throw new IllegalArgumentException("Prechod " + transition.getName() + " a miesto "
                    + this.places.get(id).getName() + " maju rovnake ID.");

        this.transitions.put(id, transition);
    }


    private void netContainsPlace(Place place)throws IllegalArgumentException {
        if (!this.places.containsValue(place))
            throw new IllegalArgumentException("Miesto " + place.getName() + " nie je v sieti.");
    }

    private void netContainsTransition(Transition transition)throws IllegalArgumentException{
        if (!this.transitions.containsValue(transition))
            throw new IllegalArgumentException("Prechod " + transition.getName() + " nie je v sieti");
    }

    @Override
    public void addPTNormalArc(Place place, Transition transition, int multiplicity) throws IllegalArgumentException {
        netContainsPlace(place);
        netContainsTransition(transition);

        NormalArc arc = new NormalArc(place, transition, multiplicity);

        this.arcs.addArc(arc);
    }

    @Override
    public void addPTNormalArc(Place place, Transition transition, int multiplicity, BreakPoint breakPoint) throws IllegalArgumentException{
        addPTNormalArc(place, transition, multiplicity);

        ArrayList<NormalArc> normalArcs = this.arcs.getNormalArcs();

        normalArcs.get(normalArcs.size() - 1).setBreakPoint(breakPoint);
    }

    @Override
    public void addPTResetArc(Place place, Transition transition) throws IllegalArgumentException {
        netContainsPlace(place);
        netContainsTransition(transition);

        ResetArc arc = new ResetArc(place, transition);

        this.arcs.addArc(arc);
    }
    @Override
    public void addPTResetArc(Place place, Transition transition, BreakPoint breakPoint) {
        NullChecker.checkNull(place);
        NullChecker.checkNull(transition);

        addPTResetArc(place, transition);

        ArrayList<ResetArc> resetArcs = this.arcs.getResetArcs();

        resetArcs.get(resetArcs.size() - 1).setBreakPoint(breakPoint);
    }

    @Override
    public void addTPNormalArc(Transition transition, Place place, int multiplicity)  {
        NullChecker.checkNull(place);
        NullChecker.checkNull(transition);

        netContainsPlace(place);
        netContainsTransition(transition);

        NormalArc arc = new NormalArc(transition, place, multiplicity);

        this.arcs.addArc(arc);
    }
    @Override
    public void addTPNormalArc(Transition transition, Place place,  int multiplicity, BreakPoint breakPoint) {
        NullChecker.checkNull(transition);
        NullChecker.checkNull(place);

        addTPNormalArc(transition, place, multiplicity);

        ArrayList<NormalArc> normalArcs = this.arcs.getNormalArcs();

        normalArcs.get(normalArcs.size() - 1).setBreakPoint(breakPoint);
    }

    public HashMap<Long, Transition> getTransitions() {
        return transitions;
    }

    @Override
    public void fireTransition(long id) throws IllegalArgumentException, NullPointerException {
        Transition transition = this.transitions.get(id);

        NullChecker.checkNull(transition);

        transition.fire();
    }
}
