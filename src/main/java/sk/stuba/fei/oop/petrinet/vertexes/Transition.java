package sk.stuba.fei.oop.petrinet.vertexes;

import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.arcs.Arc;
import sk.stuba.fei.oop.petrinet.arcs.ListArcs;
import sk.stuba.fei.oop.petrinet.arcs.NormalArc;
import sk.stuba.fei.oop.petrinet.arcs.ResetArc;

import java.util.ArrayList;


public class Transition extends Vertex implements InterfaceTransition {
    private ListArcs outArcs;
    private ListArcs inArcs;

    public Transition(long ID, String name, int x, int y) throws IllegalArgumentException {
        super(ID, name, x, y);
        this.outArcs = new ListArcs();
        this.inArcs = new ListArcs();
    }


    public ListArcs getOutArcs() {
        return outArcs;
    }

    public ListArcs getInArcs() {
        return inArcs;
    }

    public boolean isFireable(){
        ArrayList<NormalArc> normalArcs = this.inArcs.getNormalArcs();
        ArrayList<ResetArc> resetArcs = this.inArcs.getResetArcs();
        Place place;

        for(NormalArc arc : normalArcs){
            if(arc.getOut() instanceof Transition)
                continue;

            place = (Place) arc.getOut();

            if(arc.getMultiplicity() > place.getMarking() )
                return false;
        }

        for(ResetArc arc : resetArcs){
            place = (Place) arc.getOut();
            if( place.getMarking() <= 0 )
                return false;
        }

        return true;
    }

    @Override
    public void addInArc(Arc arc) throws IllegalArgumentException, NullPointerException {
        NullChecker.checkNull(arc);

        if (arc instanceof NormalArc)
            this.inArcs.addArc((NormalArc) arc);
        else if (arc instanceof ResetArc)
            this.inArcs.addArc((ResetArc) arc);
        else
            throw new IllegalArgumentException();
    }

    @Override
    public void addOutArc(Arc arc) throws IllegalArgumentException, NullPointerException {
        NullChecker.checkNull(arc);

        if (arc instanceof NormalArc)
            this.outArcs.addArc((NormalArc) arc);
        else if (arc instanceof ResetArc)
            this.outArcs.addArc((ResetArc) arc);
        else
            throw new IllegalArgumentException();
    }

    private void firePTOneArc(Place place, int multiplicity) throws IllegalArgumentException {
        NullChecker.checkNull(place);

        place.decreaseMarking(multiplicity);
    }

    private void fireTPOneArc(Place place, int multiplicity) throws IllegalArgumentException {
        NullChecker.checkNull(place);

        place.increaseMarking(multiplicity);
    }

    private void firePTArcs()throws NullPointerException {
        ArrayList<NormalArc> normalArcs = inArcs.getNormalArcs();
        ArrayList<ResetArc> resetArcs = inArcs.getResetArcs();

        for (NormalArc arc : normalArcs)
            firePTOneArc((Place) arc.getOut(), arc.getMultiplicity());

        for (ResetArc arc : resetArcs)
            firePTOneArc((Place) arc.getOut(), arc.getMultiplicity());

    }

    private void fireTPArcs()throws NullPointerException {
        ArrayList<NormalArc> normalArcs = outArcs.getNormalArcs();

        for (NormalArc arc : normalArcs)
            fireTPOneArc((Place) arc.getIn(), arc.getMultiplicity());

    }

    @Override
    public void fire()throws NullPointerException {
        firePTArcs();
        fireTPArcs();
    }
}
