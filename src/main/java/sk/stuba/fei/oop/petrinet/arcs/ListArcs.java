package sk.stuba.fei.oop.petrinet.arcs;

import sk.stuba.fei.oop.support.NullChecker;

import java.util.ArrayList;

public class ListArcs implements InterfaceListArcs {
    private ArrayList<NormalArc> normalArcs;
    private ArrayList<ResetArc> resetArcs;

    public ListArcs() {
        normalArcs = new ArrayList<NormalArc>();
        resetArcs = new ArrayList<ResetArc>();
    }

    public ArrayList<NormalArc> getNormalArcs() {
        return normalArcs;
    }

    @Override
    public void addArc(NormalArc normalArc) throws NullPointerException {
        NullChecker.checkNull(normalArc);

        this.normalArcs.add(normalArc);
    }

    public ArrayList<ResetArc> getResetArcs() {
        return resetArcs;
    }

    @Override
    public void addArc(ResetArc resetArc) throws NullPointerException {
        NullChecker.checkNull(resetArc);

        this.resetArcs.add(resetArc);
    }

}
