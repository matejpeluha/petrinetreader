package sk.stuba.fei.oop.petrinet.arcs;

import sk.stuba.fei.oop.petrinet.vertexes.Vertex;

public class NormalArc extends Arc {
    protected int multiplicity;

    public NormalArc(Vertex out, Vertex in, int multiplicity) throws IllegalArgumentException {
        super(out, in);
        setMultiplicity(multiplicity);
    }

    @Override
    public int getMultiplicity() {
        return multiplicity;
    }


    public void setMultiplicity(int multiplicity) throws IllegalArgumentException {
        if (multiplicity < 1)
            throw new IllegalArgumentException("Nasobnost hrany nesmie byt zaporna alebo nulova.");

        this.multiplicity = multiplicity;
    }
}
