package sk.stuba.fei.oop.petrinet.vertexes;

import sk.stuba.fei.oop.support.IntPoint;

public class Place extends Vertex implements InterfacePlace{
    private int marking;

    public Place(long ID, String name, int marking, int x, int y) throws IllegalArgumentException {
        super(ID, name, x , y);
        setMarking(marking);
    }

    public Place(long ID, String name, int marking, IntPoint point) throws IllegalArgumentException {
        super(ID, name, point);
        setMarking(marking);
    }

    public int getMarking() {
        return marking;
    }

    public void setMarking(int marking) throws IllegalArgumentException {
        if (marking < 0)
            throw new IllegalArgumentException("Nemozno mat zaporne znackovanie.");

        this.marking = marking;
    }

    public void incrementMarking()throws IllegalArgumentException {
        setMarking(getMarking() - 1);
    }

    public void decrementMarking()throws IllegalArgumentException {
        setMarking(getMarking() - 1);
    }

    @Override
    public void increaseMarking(int value) throws IllegalArgumentException {
        setMarking(getMarking() + value);
    }

    @Override
    public void decreaseMarking(int value) throws IllegalArgumentException {
        setMarking(getMarking() - value);
    }
}
