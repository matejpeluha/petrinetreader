package sk.stuba.fei.oop.petrinet;

import sk.stuba.fei.oop.petrinet.arcs.BreakPoint;
import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Transition;

public interface InterfacePetrinet {
    void addPlace(Place place) throws IllegalArgumentException;

    void addTransition(Transition transition) throws IllegalArgumentException;

    void addPTNormalArc(Place place, Transition transition, int multiplicity) throws IllegalArgumentException;

    void addPTNormalArc(Place place, Transition transition, int multiplicity, BreakPoint breakPoint)
            throws IllegalArgumentException, NullPointerException;

    void addPTResetArc(Place place, Transition transition) throws IllegalArgumentException;

    void addPTResetArc(Place place, Transition transition, BreakPoint breakPoint)
            throws IllegalArgumentException, NullPointerException;

    void addTPNormalArc(Transition transition, Place place, int multiplicity) throws IllegalArgumentException;

    void addTPNormalArc(Transition transition, Place place, int multiplicity, BreakPoint breakPoint)
            throws IllegalArgumentException, NullPointerException;

    void fireTransition(long id) throws IllegalArgumentException;
}
