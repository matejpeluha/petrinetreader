package sk.stuba.fei.oop.petrinet.vertexes;

public interface InterfacePlace {
    public void increaseMarking(int value) throws IllegalArgumentException;
    public void decreaseMarking(int value) throws IllegalArgumentException;
}
