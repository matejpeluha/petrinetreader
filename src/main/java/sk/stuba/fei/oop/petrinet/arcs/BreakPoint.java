package sk.stuba.fei.oop.petrinet.arcs;

import sk.stuba.fei.oop.support.IntPoint;

public class BreakPoint {
    private IntPoint point;

    public BreakPoint(IntPoint point){
        isInCanvas(point);

        this.point = point;
    }

    public BreakPoint(int x, int y) throws IllegalArgumentException{
        isInCanvas(x, y);

        this.point = new IntPoint(x, y);
    }

    private void isInCanvas(int x, int y){
        if(x < 0 || y < 0)
            throw new IllegalArgumentException("Breakpoint mimo Canvas.");
    }

    private void isInCanvas(IntPoint point){
        if(point.getX() < 0 || point.getY() < 0)
            throw new IllegalArgumentException("Breakpoint mimo Canvas.");
    }

    public int getX() {
        return this.point.getX();
    }

    public void setX(int x) {
        this.point.setX(x);
    }

    public int getY() {
        return this.point.getY();
    }

    public void setY(int y) {
        this.point.setY(y);
    }

    public IntPoint getPoint() {
        return point;
    }

    public void setPoint(IntPoint point) {
        this.point = point;
    }
}
