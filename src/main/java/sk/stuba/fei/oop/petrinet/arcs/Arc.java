package sk.stuba.fei.oop.petrinet.arcs;

import sk.stuba.fei.oop.support.NullChecker;
import sk.stuba.fei.oop.petrinet.vertexes.Place;
import sk.stuba.fei.oop.petrinet.vertexes.Transition;
import sk.stuba.fei.oop.petrinet.vertexes.Vertex;


abstract public class Arc {
    protected Vertex in;
    protected Vertex out;
    protected BreakPoint breakPoint;


    public Arc(Vertex out, Vertex in) throws IllegalArgumentException, NullPointerException {
        NullChecker.checkNull(out);
        NullChecker.checkNull(in);

        if (!areInAndOutDifferent(in, out))
            throw new IllegalArgumentException("Hranou nemozno spojit miesto s prechodom.");

        else if (this instanceof ResetArc && out instanceof Transition)
            throw new IllegalArgumentException("Reset hrana nesmie vychadzat z prechodu a smerovat do miesta.");

        this.in = in;
        this.out = out;

        if (in instanceof Transition)
            ((Transition) in).addInArc(this);
        else if (out instanceof Transition)
            ((Transition) out).addOutArc(this);
    }

    private boolean areInAndOutDifferent(Vertex in, Vertex out) {
        if (in instanceof Place && out instanceof Place)
            return false;
        else return !(in instanceof Transition) || !(out instanceof Transition);
    }

    public BreakPoint getBreakPoint() {
        return breakPoint;
    }

    public void setBreakPoint(BreakPoint breakPoint) {
        this.breakPoint = breakPoint;
    }

    public Vertex getIn() {
        return in;
    }

    public Vertex getOut() {
        return out;
    }

    public void setIn(Vertex in) {
        this.in = in;
    }

    public void setOut(Vertex out) {
        this.out = out;
    }


    abstract public int getMultiplicity();
}
