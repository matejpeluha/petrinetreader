package sk.stuba.fei.oop.petrinet.arcs;

public interface InterfaceListArcs {
    public void addArc(NormalArc normalArc) throws IllegalArgumentException;
    public void addArc(ResetArc resetArc) throws IllegalArgumentException;
}
