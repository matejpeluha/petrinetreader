package sk.stuba.fei.oop.petrinet.vertexes;

import sk.stuba.fei.oop.petrinet.arcs.Arc;

public interface InterfaceTransition {
    public void addInArc(Arc arc) throws IllegalArgumentException;
    public void addOutArc(Arc arc) throws IllegalArgumentException;
    public void fire();
}
